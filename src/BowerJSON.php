<?php

/**
 * @file
 * Contains \Drupal\bower\BowerJSON.
 */

namespace Drupal\bower;

/**
 * Class BowerJSON
 * @package Drupal\bower
 */
class BowerJSON {

  /**
   * @var string
   */
  protected $path;

  /**
   * @var array
   */
  protected $json;

  /**
   * @var array
   */
  protected $rc;

  /**
   * @var string
   */
  protected $componentDirectory;

  /**
   * Constructor.
   *
   * @param string $path
   */
  public function __construct($path) {

    // If path is a folder, we look for a bower.json inside.
    if (is_dir($path) & file_exists($path . '/bower.json')) {
      $this->path = $path . '/bower.json';
    }
    // Check if the specific file exists otherwise.
    elseif (file_exists($path)) {
      $this->path = $path;
    }
    // Otherwise we throw an error.
    else {
      throw new \Exception(t('No bower.json located at "@path".', array(
        '@path' => $path,
      )));
    }

  }

  /**
   * Provides the json file path.
   *
   * @return string
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * Provides the JSON data.
   *
   * @return array
   */
  public function getJSON() {
    if (!isset($this->json)) {
      $this->json = drupal_json_decode(file_get_contents($this->path));

      $this->json = array_merge($this->json, $this->getProcessedJSON());

      // Prepare main property to be an array.
      if (!isset($this->json['main'])) {
        $this->json['main'] = array();
      }
      elseif (!is_array($this->json['main'])) {
        $this->json['main'] = array($this->json['main']);
      }
    }
    return $this->json;
  }

  /**
   * Get additional info from the .bower.json if present.
   */
  protected function getProcessedJSON() {
    $dir = dirname($this->path);
    if (file_exists("$dir/.bower.json")) {
      return drupal_json_decode(file_get_contents($this->path));
    }
    return array();
  }

  /**
   * Provides data of the .bowerrc.
   *
   * @return array
   */
  public function getRC() {

    if (!isset($this->rc)) {

      $dir = dirname($this->path);
      if (file_exists($dir . '/.bowerrc')) {
        $rc = drupal_json_decode(file_get_contents($dir . '/.bowerrc'));
      }

      // Set rc with relevant default properties.
      $this->rc = array_merge(
        array('directory' => 'bower_components'),
        $rc
      );

    }
    return $this->rc;
  }

  /**
   * Get the name for the bower JSON.
   *
   * @return string
   */
  public function getName() {
    $json = $this->getJSON();
    return $json['name'];
  }

  /**
   * Retrieve main files for the json.
   *
   * @return array
   */
  public function getMainFiles() {

    $json = $this->getJSON();

    // If no main information is given, we have no main files.
    // @todo: add auto detection?
    if (empty($json['main'])) {
      return array();
    }


    $dir = dirname($this->path);

    $files = array();
    foreach ($json['main'] as $filename) {
      $files[] = "$dir/$filename";
    }

    return array_unique($files);
  }

  /**
   * @return array
   */
  public function getDependencies() {
    $json = $this->getJSON();

    if (isset($json['dependencies'])) {
      return array_keys($json['dependencies']);
    }
    return array();
  }

}
